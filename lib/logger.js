"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
const chalk_1 = require("chalk");
const ora_1 = require("ora");
const frameLength = 120;
class Logger {
    constructor(level) {
        this.verbose = level === 'verbose';
        this.silent = level === 'silent';
        if (!this.silent) {
            this.ora = (0, ora_1.default)({
                text: 'Starting...',
                color: 'blue',
                spinner: 'dots',
            });
            this.ora.stop();
        }
        const noop = () => { };
        this.modify = this.silent ? noop : this._modify.bind(this);
        this.write = this.silent ? noop : this._write.bind(this);
    }
    flush() {
        !this.silent && this.ora.succeed();
        return new Promise((resolve) => setTimeout(resolve, frameLength));
    }
    _write(update, color = 'green') {
        this.ora.succeed().text = chalk_1.default[color](update);
        this.ora.start();
    }
    _modify(update, color = this.ora.color) {
        this.ora.text = update;
        this.ora.color = color;
    }
    step(text, method = 'succeed') {
        if (this.silent) {
            return { modify() { }, log() { }, pause() { }, resume() { } };
        }
        if (!this.ora.id) {
            this.ora.start().text = text;
            if (method !== 'succeed') {
                this.ora[method]();
            }
        }
        else {
            this.ora[method]().text = text;
            this.ora.start();
        }
        return {
            modify: this.modify,
            log: this.verbose ? this.write : this.modify,
            pause: () => this.ora.stopAndPersist(),
            resume: () => this.ora.start(),
        };
    }
}
exports.Logger = Logger;
