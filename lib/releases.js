"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUnBuiltReleases = exports.getLatestGitRelease = void 0;
const got = require("got");
const target_1 = require("./target");
const versionsToSkip = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 15, 17, 19, 21, 22];
function getJson(url, options) {
    return __awaiter(this, void 0, void 0, function* () {
        return JSON.parse((yield got(url, options)).body);
    });
}
function isBuildableVersion(version) {
    if (version === '12.11.0') {
        return false;
    }
    return !versionsToSkip.includes(Number(version.split('.')[0]));
}
function getLatestGitRelease(options) {
    return getJson('https://api.github.com/repos/nexe/nexe/releases/latest', options);
}
exports.getLatestGitRelease = getLatestGitRelease;
function getUnBuiltReleases(options) {
    return __awaiter(this, void 0, void 0, function* () {
        const nodeReleases = yield getJson('https://nodejs.org/download/release/index.json');
        const existingVersions = (yield getLatestGitRelease(options)).assets.map((x) => (0, target_1.getTarget)(x.name));
        const versionMap = {};
        return nodeReleases
            .reduce((versions, { version }) => {
            version = version.replace('v', '').trim();
            if (!isBuildableVersion(version) || versionMap[version]) {
                return versions;
            }
            versionMap[version] = true;
            target_1.platforms.forEach((platform) => {
                target_1.architectures.forEach((arch) => {
                    if (arch === 'x86' && platform === 'mac')
                        return;
                    if (arch.includes('arm'))
                        return;
                    versions.push((0, target_1.getTarget)({ platform, arch, version }));
                });
            });
            return versions;
        }, [])
            .filter((x) => !existingVersions.some((t) => (0, target_1.targetsEqual)(t, x)));
    });
}
exports.getUnBuiltReleases = getUnBuiltReleases;
