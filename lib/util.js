"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeFileAsync = exports.isDirectoryAsync = exports.pathExistsAsync = exports.readFileAsync = exports.execFileAsync = exports.statAsync = exports.rimrafAsync = exports.isWindows = exports.bound = exports.semverGt = exports.padRight = exports.dequote = exports.wrap = exports.each = exports.STDIN_FLAG = void 0;
const fs_1 = require("fs");
const child_process_1 = require("child_process");
const util_1 = require("util");
const rimraf = require("rimraf");
const rimrafAsync = (0, util_1.promisify)(rimraf);
exports.rimrafAsync = rimrafAsync;
exports.STDIN_FLAG = '[stdin]';
function each(list, action) {
    return __awaiter(this, void 0, void 0, function* () {
        const l = yield list;
        return Promise.all(l.map(action));
    });
}
exports.each = each;
function wrap(code) {
    return '!(function () {' + code + '})();';
}
exports.wrap = wrap;
function falseOnEnoent(e) {
    if (e.code === 'ENOENT') {
        return false;
    }
    throw e;
}
function padRight(str, l) {
    return (str + ' '.repeat(l)).substr(0, l);
}
exports.padRight = padRight;
const bound = function bound(target, propertyKey, descriptor) {
    const configurable = true;
    return {
        configurable,
        get() {
            const value = descriptor.value.bind(this);
            Object.defineProperty(this, propertyKey, {
                configurable,
                value,
                writable: true,
            });
            return value;
        },
    };
};
exports.bound = bound;
function dequote(input) {
    input = input.trim();
    const singleQuote = input.startsWith("'") && input.endsWith("'");
    const doubleQuote = input.startsWith('"') && input.endsWith('"');
    if (singleQuote || doubleQuote) {
        return input.slice(1).slice(0, -1);
    }
    return input;
}
exports.dequote = dequote;
const readFileAsync = (0, util_1.promisify)(fs_1.readFile);
exports.readFileAsync = readFileAsync;
const writeFileAsync = (0, util_1.promisify)(fs_1.writeFile);
exports.writeFileAsync = writeFileAsync;
const statAsync = (0, util_1.promisify)(fs_1.stat);
exports.statAsync = statAsync;
const execFileAsync = (0, util_1.promisify)(child_process_1.execFile);
exports.execFileAsync = execFileAsync;
const isWindows = process.platform === 'win32';
exports.isWindows = isWindows;
function pathExistsAsync(path) {
    return statAsync(path)
        .then((x) => true)
        .catch(falseOnEnoent);
}
exports.pathExistsAsync = pathExistsAsync;
function isDirectoryAsync(path) {
    return statAsync(path)
        .then((x) => x.isDirectory())
        .catch(falseOnEnoent);
}
exports.isDirectoryAsync = isDirectoryAsync;
/**
 * @param version See if this version is greather than the second one
 * @param operand Version to compare against
 */
function semverGt(version, operand) {
    const [cMajor, cMinor, cPatch] = version.split('.').map(Number);
    let [major, minor, patch] = operand.split('.').map(Number);
    if (!minor)
        minor = 0;
    if (!patch)
        patch = 0;
    return (cMajor > major ||
        (cMajor === major && cMinor > minor) ||
        (cMajor === major && cMinor === minor && cPatch > patch));
}
exports.semverGt = semverGt;
