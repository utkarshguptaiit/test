"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function buildFixes(compiler, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!compiler.target.version.startsWith('8.2')) {
            return next();
        }
        const file = yield compiler.readFileAsync('./tools/msvs/find_python.cmd');
        yield compiler.replaceInFileAsync('./tools/msvs/find_python.cmd', '%p%python.exe -V 2>&1', '"%p%python.exe" -V 2>&1');
        return next();
    });
}
exports.default = buildFixes;
